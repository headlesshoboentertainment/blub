using UnityEngine;
using System.Collections;

public class CharactorSlowEffect : MonoBehaviour {

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void StartSlowEffect()
    {
        GameObject tSlowEffect = GameObject.Find("SlowEffect");
        tSlowEffect.transform.position = transform.position;
        tSlowEffect.particleSystem.Play();
    }
}
