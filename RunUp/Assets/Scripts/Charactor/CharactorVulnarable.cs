using UnityEngine;
using System.Collections;

public class CharactorVulnarable : CharactorPowerUp
{

    public float ExtraProcentInrease;

    void ActivateVulnarable()
    {
        cCanRun = true;
        CalculateDeaktiveTime();
    }

    protected override void Deaktivate()
    {
        cCanRun = false;
    }
}
