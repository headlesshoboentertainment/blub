using UnityEngine;
using System.Collections;

public class CharactorHealth : MonoBehaviour {

    public GameObject Explosion;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider pCollider)
    {
        if (pCollider.collider.tag == "Water")
        {
            Instantiate(Explosion, transform.position, Quaternion.identity);
            StartCoroutine(Die());
        }
    }

    IEnumerator Die()
    {
        yield return new WaitForSeconds(0.1f);
        GetComponent<CharactorMovement>().cCanMove = false;
        yield return new WaitForSeconds(0.8f);
        Application.LoadLevel(Application.loadedLevelName);
    }
}
