using UnityEngine;
using System.Collections;

public class CaractorSpeedUpTrail : MonoBehaviour {
    
    public GameObject TrailRenderer;

    private float mNextTimeToDeactivateTrail;

	// Use this for initialization
	void Start () {
	}

    void EnableTrailRenderer()
    {
        TrailRenderer.SetActive(true);
    }
    void DisableTrailRenderer()
    {
        TrailRenderer.SetActive(false);
    }
	
	// Update is called once per frame
	void Update () {
        ShouldDisableTrail();
	}

    void ShouldDisableTrail()
    {
        if (mNextTimeToDeactivateTrail < Time.time)
        {
            DisableTrailRenderer();
        }
    }

    void SpeedUpTrail(float pHowLongToBeActive)
    {
        EnableTrailRenderer();
        CalculateNextTimeToDeactivateTrail(pHowLongToBeActive);
    }

    IEnumerator SpeedDownTrail()
    {
        yield return new WaitForSeconds(1);
        DisableTrailRenderer();
    }

    void CalculateNextTimeToDeactivateTrail(float pTimeToDeactivate)
    {
        mNextTimeToDeactivateTrail = Time.time + pTimeToDeactivate;
    }

    
}
