using UnityEngine;
using System.Collections;

public class CharactorSprintSpeedUp : CharactorPowerUp {

    private float mSprintProcentIncrease;

    private bool mUp;
    private bool mDown;

    private bool mHasSpeedUpDown;
    private bool mHasSpeedUpUp;

	

    void SpeedUpSprint()
    {
        if (!mHasSpeedUpUp)
        {
            mHasSpeedUpUp = true;
            mCharactorMovement.SprintProcentIncrease *= 1 + (mSprintProcentIncrease / 100);
        }
    }
    void BackToNormalSprintUp()
    {
        if (mHasSpeedUpUp)
        {
            mHasSpeedUpUp = false;
            mCharactorMovement.SprintProcentIncrease /= 1 + (mSprintProcentIncrease / 100);
        }
    }


    void SpeedDownSprint()
    {
        if (!mHasSpeedUpDown)
        {
            WriteTextManager.WriteTextToPlayer("Slowed");
            mHasSpeedUpDown = true;
            mCharactorMovement.SprintProcentIncrease /= 1 + (mSprintProcentIncrease / 100);
        }
    }
    void BackToNormalSprintDown()
    {
        if (mHasSpeedUpDown)
        {
            mHasSpeedUpDown = false;
            mCharactorMovement.SprintProcentIncrease *= 1 + (mSprintProcentIncrease / 100);
        }
    }

 

  
    protected override void Deaktivate()
    {
        cCanRun = false;
        if (mUp)
        {
            mUp = false;
            if (mHasSpeedUpUp)
            {
                BackToNormalSprintUp();
            }
        }
        if (mDown)
        {
            mDown = false;
            if (mHasSpeedUpDown)
            {
                BackToNormalSprintDown();
            }
        }
        
    }

    void ActivateSprintSpeedUp(float pSprintProcentIncrease)
    {
        SendMessage("SpeedUpTrail", 1);
        cCanRun = true;
        mUp = true;
        mSprintProcentIncrease = pSprintProcentIncrease;
        if (!mHasSpeedUpUp)
        {
            SpeedUpSprint();
        }
        CalculateDeaktiveTime();
    }

    void ActivateSprintSpeedDown(float pSprintProcentIncrease)
    {
        cCanRun = true;
        mDown = true;
        mSprintProcentIncrease = pSprintProcentIncrease;
        if (!mHasSpeedUpDown)
        {
            SpeedDownSprint();
        }
        CalculateDeaktiveTime();
    }


}
