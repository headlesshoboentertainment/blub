using UnityEngine;
using System.Collections;

public class CharactorSpikeSlowDown : CharactorPowerUp {


    public string[] TagsThatSlow;

    public float ProcentSlow;

    private float mRemProcentSlow;

    private bool mHasBeenSlowed;

    private CharactorVulnarable mCharactorVulnarable;

    protected override void Start()
    {
        base.Start();
        mCharactorVulnarable = GetComponent<CharactorVulnarable>();
        mRemProcentSlow = ProcentSlow;
    }

	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider pCollider)
    {
        foreach (string tTag in TagsThatSlow)
        {
            if (pCollider.tag == tTag)
            {
                Activate();
            }
        }
    }

    protected override void Deaktivate()
    {
        if (mHasBeenSlowed)
        {
            SetCharactorBackToNormal();
        }
        cCanRun = false;
        ProcentSlow = mRemProcentSlow;
    }

    void Activate()
    {
        if (!GetComponent<CharactorSpikeSpeedUp>().cEnabled)
        {           
            cCanRun = true;
            CalculateDeaktiveTime();
            SendMessage("StartSlowEffect");
            if (!mHasBeenSlowed)
            {
                if (mCharactorVulnarable.cCanRun)
                {
                    ProcentSlow += mCharactorVulnarable.ExtraProcentInrease;
                }
                SlowCharactor();
            }
        }
    }

    void SlowCharactor()
    {
        if (!mHasBeenSlowed)
        {
            WriteTextManager.WriteTextToPlayer("Slowed");
            mHasBeenSlowed = true;
            mCharactorMovement.MinSpeed /= 1 + (ProcentSlow / 100);
            mCharactorMovement.MaxSpeed /= 1 + (ProcentSlow / 100);
        }
    }

    void SetCharactorBackToNormal()
    {
        if (mHasBeenSlowed)
        {
            mHasBeenSlowed = false;
            mCharactorMovement.MinSpeed *= 1 + (ProcentSlow / 100);
            mCharactorMovement.MaxSpeed *= 1 + (ProcentSlow / 100);
        }
    }

}
