using UnityEngine;
using System.Collections;

public class CharactorSpeedUp : CharactorPowerUp {

    private float mProcentIncrease;


    private bool mHasSpeededUp;
	
	// Update is called once per frame
	void Update () {
	}

    void SpeedUpCharactorMovement()
    {
        SendMessage("SpeedUpTrail", HowLongToBeActivated);
        mHasSpeededUp = true;
        mCharactorMovement.MinSpeed *= (1 + (mProcentIncrease));
        mCharactorMovement.MaxSpeed *= (1 + (mProcentIncrease));
    }

    void SetSpeedToNormalCharactorMovement()
    {
        mHasSpeededUp = false;
        mCharactorMovement.MinSpeed /= (1 + (mProcentIncrease));
        mCharactorMovement.MaxSpeed /= (1 + (mProcentIncrease));
    }

    public void ActivateSpeedUp(float pProcentIncrease)
    {
        cCanRun = true;
        mProcentIncrease = pProcentIncrease / 100;
        SpeedUpCharactorMovement();
        CalculateDeaktiveTime();
    }

    public void SpeedUp(float pProcentIncrease)
    {
        mProcentIncrease = pProcentIncrease / 100;
        if (!mHasSpeededUp)
        {
            SpeedUpCharactorMovement();
        }
    }

    public void BackToNormal()
    {
        if (mHasSpeededUp)
        {
            SetSpeedToNormalCharactorMovement();
        }
    }

    protected override void Deaktivate()
    {
        cCanRun = false;
        if (mHasSpeededUp)
        {
            SetSpeedToNormalCharactorMovement();
        }
        mProcentIncrease = 0;
    }
}
