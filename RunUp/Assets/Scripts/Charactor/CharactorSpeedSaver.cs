using UnityEngine;
using System.Collections;

public class CharactorSpeedSaver : CharactorPowerUp {

    public float TimeBeingActive;

    private bool mEnabled;
    private bool mHasBeenSpeededUp;

    private float mSpeedProcentIncrease;

    private float mNextTimeToDeactivateSpeedIncrease;

	// Use this for initialization
	
	
	// Update is called once per frame
	void Update () {
        if (cCanRun && mHasBeenSpeededUp)
        {
            ShouldDeactivate();
        }
        
	}

    protected override void FixedUpdate()
    {
        
    }

    void SpeedUpCharactorMovement()
    {
        mHasBeenSpeededUp = true;
        mCharactorMovement.MinSpeed *= (1 + (mSpeedProcentIncrease / 100));
        mCharactorMovement.MaxSpeed *= (1 + (mSpeedProcentIncrease / 100));
    }

    void SetSpeedToNormalCharactorMovement()
    {
        mHasBeenSpeededUp = false;
        mCharactorMovement.MinSpeed /= (1 + (mSpeedProcentIncrease / 100));
        mCharactorMovement.MaxSpeed /= (1 + (mSpeedProcentIncrease / 100));
    }

    void ShouldDeactivate()
    {
        if (mNextTimeToDeactivateSpeedIncrease < Time.time)
        {
            SpeedSaverDeactivate();
        }
    }

    void SpeedSaverDeactivate()
    {
        SetSpeedToNormalCharactorMovement();
    }

    void ActivateSpeedSaver(float pSpeedIncreaseProcent)
    {
        mSpeedProcentIncrease = pSpeedIncreaseProcent;
        cCanRun = true;
        CalculateDeaktiveTime();
    }

  

    protected override void Deaktivate()
{
    cCanRun = false;
     mSpeedProcentIncrease = 0;
}
   

    void SpeedSaver()
    {
        if (cCanRun)
        {
            if (!mHasBeenSpeededUp)
            {
                SpeedUpCharactorMovement();
            }
            
            CalculateNextTimeDeactiveSpeedSaver();
        }
    }

    void CalculateNextTimeDeactiveSpeedSaver()
    {
        mNextTimeToDeactivateSpeedIncrease = Time.time + TimeBeingActive;
    }
}
