using UnityEngine;
using System.Collections;

public class CharactorDoubleJump : CharactorPowerUp {

    public float HeightOverGroundBeforeDoubleJump;

    private bool mDoubleJump;

    private bool mCanDoubleJump;

	// Use this for initialization
	protected override void Start () {
        base.Start();
	}
	
	// Update is called once per frame
     void Update()
    {
        if (mCanDoubleJump)
        {
            DoubleJump();
        }
	}

     protected override void Deaktivate()
     {
         mCanDoubleJump = false;
     }

     void DoubleJump()
     {
        
         if (mCharactorMovement.cJumping && Input.GetButtonDown("Jump") && !mDoubleJump && 
             !Physics.Raycast(transform.position, Vector3.down, transform.lossyScale.y / 2 + HeightOverGroundBeforeDoubleJump))
         {
             mCharactorMovement.Jump();
             mDoubleJump = true;
         }
         if (!mCharactorMovement.cJumping)
         {
             mDoubleJump = false;
         }
     }

     void ActivateDoubleJump()
     {
         mCanDoubleJump = true;
         cCanRun = true;
         CalculateDeaktiveTime();
     }
}
