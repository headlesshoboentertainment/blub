using UnityEngine;
using System.Collections;

public class CharactorAnimation : MonoBehaviour {

    private AnimationsManager mAnimationsManager;

    private CharactorMovement mCharactorMovement;

    private bool mAnimationStartedToIdle;
    private bool mAnimationStartedToRunning;
    private bool mLastRunning;
    private bool mLastIdle;
    

	// Use this for initialization
	void Start () {
        mAnimationsManager = GetComponent<AnimationsManager>();
        mCharactorMovement = GetComponent<CharactorMovement>();
        mAnimationStartedToIdle = false;
        mAnimationStartedToRunning = true;
        }
	
	// Update is called once per frame
	void Update () {
        Changed();
            Idle();
            Running();

        
       
	}

    void Changed()
    {
        if (mLastIdle != mCharactorMovement.cIdle && mCharactorMovement.cIdle && mLastIdle != mCharactorMovement.cIdle)
        {
            mAnimationStartedToIdle = false;
        }
        if (!mLastRunning && mCharactorMovement.cMoving && mLastRunning != mCharactorMovement.cMoving)
        {
            mAnimationStartedToRunning = false;
        }

        mLastIdle = mCharactorMovement.cIdle;
        mLastRunning = mCharactorMovement.cMoving;
    }

    void Running()
    {
        if (!mAnimationStartedToRunning)
        {
            mAnimationStartedToRunning = true;
            mAnimationsManager.StopAnimation();
            mAnimationsManager.StartAnimationBasedOnName("Running");
        }
    }

    void Idle()
    {
        if (!mAnimationStartedToIdle)
        {
            mAnimationStartedToIdle = true;
            mAnimationsManager.StopAnimation();
            mAnimationsManager.StartAnimationBasedOnName("Idle");
        }
    }
}
