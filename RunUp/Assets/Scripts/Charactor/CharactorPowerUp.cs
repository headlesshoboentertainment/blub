using UnityEngine;
using System.Collections;

public class PowerInfo
{
    public float PowerToSave;
    public float Time;
    public bool HasDoneThing;
}


public class CharactorPowerUp : MonoBehaviour {

    public float HowLongToBeActivated;

    protected CharactorMovement mCharactorMovement;

    public bool cCanRun { get; set; }

    protected float mTimeToDeaktivate;

    protected PowerInfo mPowerInfos;

	// Use this for initialization
	protected virtual void Start () 
    {
        mCharactorMovement = GetComponent<CharactorMovement>();
	}
	
	// Update is called once per frame
    protected virtual void FixedUpdate()
    {
        if (cCanRun)
        {
            ShouldDeaktivate();
        }
	}

    public void Disable()
    {
        mTimeToDeaktivate = float.MinValue;   
    }

    public virtual void DisableAll()
    {
        BroadcastMessage("Disable",SendMessageOptions.DontRequireReceiver);
    }

    protected void ShouldDeaktivate()
    {
        if(mTimeToDeaktivate < Time.time)
        {
            Deaktivate();
        }
    }

    protected virtual void Deaktivate()
    {

    }

    protected virtual void Activate()
    {        
        cCanRun = true;
        CalculateDeaktiveTime();        
    }

   
    

    protected void CalculateDeaktiveTime()
    {
        DisableAll();
        mTimeToDeaktivate = Time.time + HowLongToBeActivated;
    }
        

}
