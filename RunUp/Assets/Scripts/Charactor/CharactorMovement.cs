using UnityEngine;
using System.Collections;

public class CharactorMovement : MonoBehaviour
{
    public float MaxSpeed = 8.0F;
    public float MinSpeed = 4.0F;
    public float AccelerationSpeed = 5.0F;
    public float JumpSpeed = 8.0F;
    public float Gravity = 20.0F;
    public float JumpGravity = 20.0F;
    public float SprintProcentIncrease;

    private float mCurrentSpeed;

    public bool cJumping { get; set; }
    public bool cMoving { get; set; }
    public bool cIdle { get; set; }
    public bool cCanMove;

    private bool mHasSprinted;

    private float mHorizontal;

    private Vector3 mStartScale;

    private Vector3 mMoveDirection = Vector3.zero;

    private Vector2 mStartSpeeds;

    void Start()
    {
        cCanMove = true;
        mCurrentSpeed = MinSpeed;
        mStartScale = transform.lossyScale;
        mStartSpeeds = new Vector2(MinSpeed, MaxSpeed);
        
    }
    void FixedUpdate()
    {
        if (cCanMove)
        {
            MovingStates();
            Move();
        }
        
        
    }

    public void ResetSpeeds()
    {
        MinSpeed = mStartSpeeds.x;
        MaxSpeed = mStartSpeeds.y;
    }

    public void Jump()
    {
        mMoveDirection.y = JumpSpeed;
        cJumping = true;
    }

    void Move()
    {
        CharacterController controller = GetComponent<CharacterController>();
        mHorizontal = Input.GetAxis("Horizontal");
        mMoveDirection.x = -mHorizontal;
        TurnTheRightWay();
        mMoveDirection = transform.TransformDirection(mMoveDirection);
        mMoveDirection.x *= mCurrentSpeed;

        if (controller.isGrounded)
        {
            cJumping = false;
            if (Input.GetButton("Jump"))
            {
                Jump();
            }

        }

        if (Input.GetButton("Horizontal"))
        {
            if (mCurrentSpeed < MaxSpeed)
            {
                AccCurrentSpeed();
            }
        }

        else if (!Input.GetButton("Horizontal"))
        {
            if (mCurrentSpeed > MinSpeed)
            {
                DeAccCurrentSpeed();
            }
        }

        if (Input.GetButtonDown("Sprint"))
        {
            if (!mHasSprinted)
            {
                IntoSprint();
            }
        }
        else if (Input.GetButtonUp("Sprint"))
        {
            if (mHasSprinted)
            {
            OutOfSprint();
            }
        }
        if(Input.GetButton("Sprint"))
        {
            
                SprintTrail();
            
        }

        if (cJumping)
        {
            mMoveDirection.y -= JumpGravity * Time.deltaTime;
        }
        else
        {
            mMoveDirection.y -= Gravity * Time.deltaTime;
        }

        controller.Move(mMoveDirection * Time.deltaTime);
    }

    void IntoSprint()
    {
        if (!mHasSprinted)
        {
            mHasSprinted = true;
            MaxSpeed *= (float)(1 + (float)((float)SprintProcentIncrease / (float)100f));
            MinSpeed *= (float)(1 + (float)((float)SprintProcentIncrease / (float)100f));
        }
    }

    void SprintTrail()
    {
        SendMessage("SpeedUpTrail", .1f);
    }

    void OutOfSprint()
    {
        if (mHasSprinted)
        {
            mHasSprinted = false;
            MaxSpeed /= (float)(1 + (float)((float)SprintProcentIncrease / (float)100f));
            MinSpeed /= (float)(1 + (float)((float)SprintProcentIncrease / (float)100f));
        }
    }

    void TurnTheRightWay()
    {
        if (mHorizontal > 0f)
        {
            transform.localScale = new Vector3(mStartScale.x, mStartScale.y, mStartScale.z); 
        }
        else if (mHorizontal < 0)
        {
            transform.localScale = new Vector3(-mStartScale.x, mStartScale.y, mStartScale.z); 
        }
    }

    void MovingStates()
    {
        if (mHorizontal != 0)
        {
            cMoving = true;
            cIdle = false;
        }
        else if (mHorizontal == 0)
        {
            cMoving = false;
            cIdle = true;
        }
    }

    void AccCurrentSpeed()
    {
        mCurrentSpeed += (AccelerationSpeed * Time.deltaTime);
    }
    void DeAccCurrentSpeed()
    {
        mCurrentSpeed -= (AccelerationSpeed * Time.deltaTime);
    }
}