using UnityEngine;
using System.Collections;

public class CharactorSpikeSpeedUp : CharactorPowerUp {


    public string[] TagsThatSpeedUp;

    public float ProcentSpeedUp;
    public float HowLongSpeedUpLast;

    private bool mHasBeenSpeededUp;
    public bool cEnabled { get; set; }

    private float mNextTimeToDeacktivate;
	
	// Update is called once per frame
	void Update () {
	IsDeactivationTime();
	}

    void OnTriggerEnter(Collider pCollider)
    {
        foreach (string tTag in TagsThatSpeedUp)
        {
            if (pCollider.tag == tTag)
            {
                Activate();
            }
        }
    }

    void IsDeactivationTime()
    {
        if(mNextTimeToDeacktivate < Time.time)
        {
            cEnabled = false;
        }
    }

    protected override void Deaktivate()
    {
        if (mHasBeenSpeededUp)
        {
            SetCharactorBackToNormal();
        }
        cCanRun = false;
    }

    void CalculateTimeToDeactivate()
    {
        mNextTimeToDeacktivate = Time.time + HowLongSpeedUpLast;
    }

    void ActivateSpikeSpeedUp()
    {
        cEnabled = true;
        CalculateTimeToDeactivate();
    }

    void Activate()
    {
        if (cEnabled)
        {
            SendMessage("SpeedUpTrail", 1);
            cCanRun = true;
            CalculateDeaktiveTime();
            if (!mHasBeenSpeededUp)
            {
                SpeedUpCharactor();
            }
        }
    }

    void SpeedUpCharactor()
    {
        
        mHasBeenSpeededUp = true; 
        mCharactorMovement.MinSpeed *= 1 + (ProcentSpeedUp / 100);
        mCharactorMovement.MaxSpeed *= 1 + (ProcentSpeedUp / 100);
    }

    void SetCharactorBackToNormal()
    {
        mHasBeenSpeededUp = false;
        mCharactorMovement.MinSpeed /= 1 + (ProcentSpeedUp / 100);
        mCharactorMovement.MaxSpeed /= 1 + (ProcentSpeedUp / 100);
    }

}


