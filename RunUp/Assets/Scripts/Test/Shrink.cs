using UnityEngine;
using System.Collections;

public enum Direction
{
    ShrinkLeft,
    ShrinkRight,
    ShrinkUp,
    ShrinkDown,
    Nothing
}

public  class Shrink : MonoBehaviour 
{


    private static float mCurrentShrinkAmount;
    private float mWantedShrinkAmount;
    private float mShrinkAmount;
    private static float mProcent;
    private static float mSpeed;

    private static bool mRun;

    private static Transform mTransformToChange;

    private static Direction mDirection;


	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        if (mRun)
        {
            ChooseDirection();
        }
	}



    void Reset()
    {
        mTransformToChange = null;
        mDirection = Direction.Nothing;
        mSpeed = 0;
        mProcent = 0;
        mRun = false;
    }

    public static void Init(Transform pTransformToChange,Direction pDirection,float pSpeed,float pProcent)
    {
        mTransformToChange = pTransformToChange;
        mDirection = pDirection;
        mSpeed = pSpeed;
        mProcent = pProcent;
        mRun = true;
        mCurrentShrinkAmount = 0;
    }

    void ChooseDirection()
    {
        if (mDirection == Direction.ShrinkUp)
        {
            ShrinkUp();
        }

        else if (mDirection == Direction.ShrinkDown)
        {
            ShrinkDown();
        }

        else if (mDirection == Direction.ShrinkLeft)
        {
            ShrinkLeft();
        }

        else if (mDirection == Direction.ShrinkRight)
        {
            ShrinkRight();
        }
    }



    void ShrinkUp()
    {
        mShrinkAmount = mTransformToChange.lossyScale.y;

        mWantedShrinkAmount = mShrinkAmount * (mProcent / 100);
        float tShrinkAmout = (mShrinkAmount * (mSpeed / 1000));

        if ((mCurrentShrinkAmount + tShrinkAmout) < mWantedShrinkAmount)
        {
            mTransformToChange.localScale = new Vector3(mTransformToChange.lossyScale.x, mTransformToChange.lossyScale.y - tShrinkAmout, mTransformToChange.lossyScale.z);
            mTransformToChange.position = new Vector3(mTransformToChange.position.x, mTransformToChange.position.y + tShrinkAmout / 2, mTransformToChange.position.z);
            mCurrentShrinkAmount += tShrinkAmout;
        }
        else
        {
            Reset();
        }
    }

    void ShrinkDown()
    {
        mShrinkAmount = mTransformToChange.transform.lossyScale.y;

        Transform tTransform = mTransformToChange.transform;

        mWantedShrinkAmount = mShrinkAmount * (mProcent / 100);
        float tShrinkAmout = (mShrinkAmount * (mSpeed / 1000));

        if ((mCurrentShrinkAmount + tShrinkAmout) < mWantedShrinkAmount)
        {
            mTransformToChange.localScale = new Vector3(mTransformToChange.lossyScale.x, mTransformToChange.lossyScale.y - tShrinkAmout, mTransformToChange.lossyScale.z);

            mTransformToChange.position = new Vector3(mTransformToChange.position.x, mTransformToChange.position.y - tShrinkAmout / 2, mTransformToChange.position.z);
            mCurrentShrinkAmount += tShrinkAmout;
        }
        else
        {
            Reset();
        }
    }

    void ShrinkLeft()
    {
        mShrinkAmount = mTransformToChange.lossyScale.x;

        mWantedShrinkAmount = mShrinkAmount * (mProcent / 100);
        float tShrinkAmout = (mShrinkAmount * (mSpeed / 1000));

        if ((mCurrentShrinkAmount + tShrinkAmout) < mWantedShrinkAmount)
        {
            mTransformToChange.localScale = new Vector3(mTransformToChange.lossyScale.x - tShrinkAmout, mTransformToChange.lossyScale.y, mTransformToChange.lossyScale.z);

            mTransformToChange.position = new Vector3(mTransformToChange.position.x - tShrinkAmout / 2, mTransformToChange.position.y, mTransformToChange.position.z);
            mCurrentShrinkAmount += tShrinkAmout;
        }
        else
        {
            Reset();
        }
    }

    void ShrinkRight()
    {
        mShrinkAmount = mTransformToChange.lossyScale.x;

        mWantedShrinkAmount = mShrinkAmount * (mProcent / 100);
        float tShrinkAmout = (mShrinkAmount * (mSpeed / 1000));

        if ((mCurrentShrinkAmount + tShrinkAmout) < mWantedShrinkAmount)
        {
            mTransformToChange.localScale = new Vector3(mTransformToChange.lossyScale.x - tShrinkAmout, mTransformToChange.lossyScale.y, mTransformToChange.lossyScale.z);

            mTransformToChange.position = new Vector3(mTransformToChange.position.x + tShrinkAmout / 2, mTransformToChange.position.y, mTransformToChange.position.z);
            mCurrentShrinkAmount += tShrinkAmout;
        }
        else
        {
            Reset();
        }
    }

    

  
}
