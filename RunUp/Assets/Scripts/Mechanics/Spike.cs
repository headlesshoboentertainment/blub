using UnityEngine;
using System.Collections;

public class Spike : MonoBehaviour {

    private Theme mTheme;

    void TheTheme(Theme pTheme)
    {
        mTheme = pTheme;
    }

    void Update()
    {
        UpdateMaterials();
    }

    void UpdateMaterials()
    {
        if (mTheme == Theme.Fire)
        {
            GetComponent<MeshRenderer>().enabled = false;
        }
        else if(mTheme != Theme.Fire)
        {
            GetComponent<MeshRenderer>().enabled = true;
        }
    }
}
