using UnityEngine;
using System.Collections;

public class HealthBar : MonoBehaviour {

    public float HealthBarXScale;
    public float Speed;

    private float mWantedGrowAmount = 0;
    private float mCurrentGrowAmount = 0;
    private float mProcent = 0;

    private float mWantedShrinkAmount;
    private float mCurrentShrinkAmount;

    

    private bool mRunToGrow;
    private bool mRunToShrink;

	// Use this for initialization
	void Start () {
        mCurrentGrowAmount = 0;
	}
	
	// Update is called once per frame
	void Update () {
        
        if (mRunToGrow)
        {
            GrowBar();
        }
        if (mRunToShrink)
        {
            ShrinkBar();
        }
        ShouldShrinkToZero();
	}

    void ShouldShrinkToZero()
    {
        if (mProcent >= 100 && !mRunToGrow)
        {
            ShrinkToZero();
            mProcent = 0;
        }
    }

    public void ShrinkToZero()
    {
        mRunToShrink = true;
        mWantedShrinkAmount = HealthBarXScale;
    }

    public void Grow(int tProcent)
    {
        mProcent += tProcent;
            
            mWantedGrowAmount = (HealthBarXScale * (float)((float)tProcent / (float)100f));
            mRunToGrow = true;
    }

    public void Shrink(int tProcent)
    {
        mProcent -= tProcent;
        if (mProcent >= 0)
        {
            mWantedShrinkAmount = (HealthBarXScale * (float)((float)tProcent / (float)100f));
            mRunToShrink = true;
        }
    }

    void ShrinkBar()
    {
        float tShrinkAmout = (HealthBarXScale * (Speed / 1000));

        if ((mCurrentShrinkAmount + tShrinkAmout) <= mWantedShrinkAmount)
        {            
            transform.localScale = new Vector3(transform.lossyScale.x - tShrinkAmout, transform.lossyScale.y, transform.lossyScale.z);
            transform.position = new Vector3(transform.position.x - tShrinkAmout / 2, transform.position.y, transform.position.z);
            mCurrentShrinkAmount += tShrinkAmout;
        }
        else
        {
            ShrinkReset();
        }
    }

    void GrowReset()
    {
        mCurrentGrowAmount = 0;
        mRunToGrow = false;  
    }

    void ShrinkReset()
    {
        
        mCurrentShrinkAmount = 0;
        mRunToShrink = false;
        mRunToGrow = false;
    }



    void GrowBar()
    {
        float tGrowAmout = (HealthBarXScale * (Speed / 1000));

        
        if ((mCurrentGrowAmount + tGrowAmout) <= mWantedGrowAmount)
        {
            transform.localScale = new Vector3(transform.lossyScale.x + tGrowAmout, transform.lossyScale.y, transform.lossyScale.z);
            transform.position = new Vector3(transform.position.x + tGrowAmout / 2, transform.position.y, transform.position.z);
            mCurrentGrowAmount += tGrowAmout;
        }
        else
        {
            GrowReset();
        }
    }
}
