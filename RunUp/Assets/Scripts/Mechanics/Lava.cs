using UnityEngine;
using System.Collections;

public class Lava : MonoBehaviour {

    public int PowerToTakeOnImpact;

    public float TimeBetweenTakingPower;

    private PowerManager mPowerManager;

    private bool mCanTakePower;

    private float mNextTimeToTakePower;

	// Use this for initialization
	void Start () {
        mPowerManager = GameObject.Find("PowerManager").GetComponent<PowerManager>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider pCollider)
    {
        if (pCollider.tag == "Player" && mCanTakePower && CanTakePower())
        {
            WriteTextManager.WriteTextToPlayer("Slowed -6 power");
            mCanTakePower = false;
            TakePower();
            CalculateNextTimeToTakePower();
        }
    }

    bool CanTakePower()
    {
        bool tCanTakePower = false;

        if (mNextTimeToTakePower < Time.time)
        {
            tCanTakePower = true;
        }

        return tCanTakePower;
    }


    void CalculateNextTimeToTakePower()
    {
        mNextTimeToTakePower = Time.time + TimeBetweenTakingPower;
    }

       void OnTriggerExit(Collider pCollider)
    {
        if (pCollider.tag == "Player")
        {
            mCanTakePower = true;
        }
    }

       void TakePower()
       {
           mPowerManager.RemovePower(PowerToTakeOnImpact);
       }
}
