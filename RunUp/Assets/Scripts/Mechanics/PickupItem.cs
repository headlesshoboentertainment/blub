using UnityEngine;
using System.Collections;

public enum TOP
{
    Tag,
    Name
}

[System.Serializable]
public class PickupInfo
{
    public TOP TagOrName;
    public string ObjectNameOrTag;
    public string MethodToCall;
}


public class PickupItem : MonoBehaviour {

    public string TextToPlayer;

    public PickupInfo[] PickupInfos; 


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    protected virtual void OnTriggerEnter(Collider pCollider)
    {
        if (pCollider.tag == "Player")
        {
            WriteTextToPlayer();

             foreach (PickupInfo tPickupInfo in PickupInfos)
                    {
                        if (tPickupInfo.TagOrName == TOP.Tag)
                        {
                        GameObject.FindGameObjectWithTag(tPickupInfo.ObjectNameOrTag).SendMessage(tPickupInfo.MethodToCall,SendMessageOptions.DontRequireReceiver);

                        }


                        else if (tPickupInfo.TagOrName == TOP.Name)
                        {

                            GameObject.Find(tPickupInfo.ObjectNameOrTag).SendMessage(tPickupInfo.MethodToCall, SendMessageOptions.DontRequireReceiver);
                    
                        }
                    }
             Die();
            }
           
        }

    protected virtual void WriteTextToPlayer()
    {
        WriteTextManager.WriteTextToPlayer(TextToPlayer);
    }
    

    protected virtual void Die()
    {
        Destroy(gameObject);
    }
}
