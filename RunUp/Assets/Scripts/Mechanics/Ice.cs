using UnityEngine;
using System.Collections;

public class Ice : MonoBehaviour {

    public float SpeedIncreaseProcent;

    private Material mIceBlock;


    private Theme mTheme;

	// Use this for initialization
	void Start () {
        mIceBlock = renderer.material;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void TheTheme(Theme pTheme)
    {
        mTheme = pTheme;
        if (mTheme == Theme.Frost)
        {
            SetMaterial();
        }
    }

    void SetMaterial()
    {
        renderer.material = mIceBlock;
    }

    void OnTriggerEnter(Collider pCollider)
    {
        if (pCollider.tag == "Player" && mTheme == Theme.Frost)
        {
            GameObject tCharactor = pCollider.gameObject;
            tCharactor.GetComponent<CharactorSpeedUp>().SpeedUp(SpeedIncreaseProcent);
        }
    }

    void OnTriggerExit(Collider pCollider)
    {
        if (pCollider.tag == "Player" && mTheme == Theme.Frost)
        {
            GameObject tCharactor = pCollider.gameObject;
            tCharactor.GetComponent<CharactorSpeedUp>().BackToNormal();
        }
    }
}
