using UnityEngine;
using System.Collections;

public class RunningTile : MonoBehaviour {

    public Material FrostMaterial;
    public Material FireMaterial;

    public float SpeedIncreaseProcent;

    private Theme mTheme;

    private bool mHasStartedAnimationsManager;

    private AnimationsManager mAnimationsManager;

	// Use this for initialization
	void Start () {
        mAnimationsManager = GetComponent<AnimationsManager>();
	}
	
	// Update is called once per frame
	void Update () {
        Change();
	}

    void Change()
    {
        if (renderer.isVisible)
        {
            if (mTheme == Theme.Industry && !mHasStartedAnimationsManager)
            {
                mHasStartedAnimationsManager = true;
                mAnimationsManager.StartAnimationBasedOnName("Industry");
            }
            else if (mTheme == Theme.Fire)
            {
                mHasStartedAnimationsManager = false;
                mAnimationsManager.StopAnimation();
                renderer.material = FireMaterial;
            }
            else if (mTheme == Theme.Frost)
            {
                mHasStartedAnimationsManager = false;
                mAnimationsManager.StopAnimation();
                renderer.material = FrostMaterial;
            }
        }
    }

    void TheTheme(Theme pTheme)
    {
        mTheme = pTheme;
        
    }

    void OnTriggerEnter(Collider pCollider)
    {
        if (pCollider.tag == "Player" && mTheme == Theme.Industry)
        {
            GameObject tCharactor = pCollider.gameObject;
            tCharactor.GetComponent<CharactorSpeedUp>().SpeedUp(SpeedIncreaseProcent);
        }
    }

    void OnTriggerExit(Collider pCollider)
    {
        if (pCollider.tag == "Player" && mTheme == Theme.Industry)
        {
            GameObject tCharactor = pCollider.gameObject;
            tCharactor.GetComponent<CharactorSpeedUp>().BackToNormal();
        }
    }
}
