using UnityEngine;
using System.Collections;

public class PowerDrop : CharactorPowerUp {

    public int PowerToRemove;

    private PowerManager mPowerManager;

	// Use this for initialization
	void Start () {
        mPowerManager = GameObject.Find("PowerManager").GetComponent<PowerManager>();
	}
	
	// Update is called once per frame
	void Update () {

	}

    protected override void Activate()
    {
        base.Activate();
        DisableAll();
        mPowerManager.RemovePower(PowerToRemove);
    }

}
