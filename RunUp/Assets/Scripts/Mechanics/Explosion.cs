using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour {

   

    public float GrowCollsion;
    public float HowOftenToDealDamageToEnemy;

    public int Damage;

    private float mTimeToDie;


	// Use this for initialization
	void Start () {
	    mTimeToDie = GetComponent<ParticleSystem>().startLifetime + Time.time;
	}
	
	// Update is called once per frame
	void Update () {
        if(mTimeToDie < Time.time)
        {
            Destroy(gameObject);
        }
      

	}

 

}
