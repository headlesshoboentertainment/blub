using UnityEngine;
using System.Collections.Generic;

public enum Power
{
    Down,
    Up
}

public enum PowerUp
{
    DoubleJump,
    WaterFreeze,
    SpeedFreak,
    Masochist,
    Nothing
}

public enum PowerDown
{
    Drowzy,
    Vulnarable,
    PowerDrop,
    SpeedSaver,
    Nothing
}

public class SpawnPowerItem : MonoBehaviour {

    public float AppearingChance; 
    public float PowerUpChance;
    public float PowerDownChance;

    public List<GameObject>PowerUps;
    public List<GameObject> PowerDowns;

    private Power mPower;
    private PowerUp mPowerUp;
    private PowerDown mPowerDown;

	// Use this for initialization
    protected virtual void Start()
    {
        mPowerDown = PowerDown.Nothing;
        mPowerUp = PowerUp.Nothing;
        if (Appear())
        {
            WhichPower();
            ChooseMyPower();
            SpawnPower();
        }
        else
        {
            Die();
        }
	}

    void Die()
    {
        Destroy(gameObject);
    }


    bool Appear()
    {
        bool tAppear = false;

        float tChance = CalculateChance();
        if (0 < tChance && tChance < AppearingChance)
        {
            tAppear = true;
        }

        return tAppear;
    }

    void ChooseMyPower()
    {
        if(mPower == Power.Up)
        {
            while(mPowerUp == PowerUp.Nothing)
            {
            int tRandomEnumNumber = Random.Range(0, PowerUp.GetNames(typeof(PowerUp)).Length);
            mPowerUp = (PowerUp)tRandomEnumNumber;
            mPowerDown = PowerDown.Nothing;
            }
            
        }

        else if (mPower == Power.Down)
        {
            while (mPowerDown == PowerDown.Nothing)
            {
                int tRandomEnumNumber = Random.Range(0, PowerDown.GetNames(typeof(PowerDown)).Length);
                mPowerDown = (PowerDown)tRandomEnumNumber;
                mPowerUp = PowerUp.Nothing;
            }
        }
    }

    void WhichPower()
    {
        float tChance = CalculateChance();
        if(0 < tChance && tChance < PowerDownChance)
        {
            mPower = Power.Down;
        }
        else if (PowerDownChance < tChance && tChance < (PowerDownChance + PowerUpChance))
        {
            mPower = Power.Up;
        }
    }

    float CalculateChance()
    {
        float tChance = 0;

        tChance = Random.Range(1, 100);

        return tChance;
    }

    void SpawnPower()
    {
        GameObject tMyPower = FindPower();
        Instantiate(tMyPower, transform.position, transform.rotation);
    }

    GameObject FindPower()
    {
        GameObject tPower = null;

        if (mPowerUp != PowerUp.Nothing)
        {
            tPower = PowerUps.Find(item => item.name == mPowerUp.ToString());
        }
        else if (mPowerDown != PowerDown.Nothing)
        {
            tPower = PowerDowns.Find(item => item.name == mPowerDown.ToString());
        }

        return tPower;
    }
	
	// Update is called once per frame
	protected virtual void Update () 
    {
	    
	}


}
