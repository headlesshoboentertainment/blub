using UnityEngine;
using System.Collections;

public class WaterFreeze : CharactorPowerUp {

    public float TimeBetweenWaterFreezes;

    private water mWater;

    private bool mCanFreeze;

	// Use this for initialization
	protected override void Start () {
        base.Start();
        mWater = GameObject.FindGameObjectWithTag("Water").GetComponent<water>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    protected override void Deaktivate()
    {
        cCanRun = false;
        mCanFreeze = false;
    }

    void WaterActivate()
    {
        cCanRun = true;
        CalculateDeaktiveTime();
        mCanFreeze = true;
    }

    void FreezeTheWater()
    {
        StartCoroutine(FreezeWater());
    }

    IEnumerator FreezeWater()
    {
        if (mCanFreeze)
        {
            mWater.cMove = false;
            yield return new WaitForSeconds(TimeBetweenWaterFreezes);
            mWater.cMove = true;
        }
    }

    
}
