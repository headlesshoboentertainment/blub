using UnityEngine;
using System.Collections;

public class water : MonoBehaviour {

    public float Speed;
    public float SpeedIncrease;

    private TenSecManager TenSecManager;

    public bool cMove { get; set; }

    private float mSpeed;

    private Vector3 cMoveDirection = Vector3.zero;

	// Use this for initialization
	void Start () {
    TenSecManager = GameObject.FindGameObjectWithTag("TenSecManager").GetComponent<TenSecManager>();
    TenSecManager.Register(gameObject);
    cMove = false;
    mSpeed = Speed;
	}
	
	// Update is called once per frame
	void Update () {
        if (cMove)
        {
            Move();
        }
	}

    void Move()
    {
        cMoveDirection = new Vector3(0, Speed * Time.deltaTime, 0);

        transform.Translate(cMoveDirection);
    }

    void OnTriggerEnter(Collider pCollider)
    {
        if (pCollider.tag == "Normal")
        {
            Destroy(pCollider.gameObject,0.5f);
        }
    }

    void TenSec()
    {
        if (!cMove)
        {
            cMove = true;
        }
        else
        {
            mSpeed += SpeedIncrease;
        }
       
    }
}
