using UnityEngine;
using System.Collections;

public class Fire : MonoBehaviour {

   
    public float IntervalBetweenTakingPower;

    public int PowerToTake;

    private float mNextTimeToTakePower;

    private Theme mTheme;

    private PowerManager mPowerManager;

	// Use this for initialization
	void Start () {
        mPowerManager = GameObject.Find("PowerManager").GetComponent<PowerManager>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerStay(Collider pCollider)
    {
        if (pCollider.tag == "Player" && mTheme == Theme.Fire)
        {
            if (CanTakePower())
            {
                WriteTextManager.WriteTextToPlayer("-4 power");
                TakePower();
                CalculateTimeBetweenTakingPower();
            }
        }
    }

    void TheTheme(Theme pTheme)
    {
        mTheme = pTheme;
    }

    void TakePower()
    {
        mPowerManager.RemovePower(PowerToTake);
    }

    bool CanTakePower()
    {
        bool tCanTakePower = false;

        if (mNextTimeToTakePower < Time.time)
        {
            tCanTakePower = true;
        }

        return tCanTakePower;
    }


    void CalculateTimeBetweenTakingPower()
    {
        mNextTimeToTakePower = Time.time + IntervalBetweenTakingPower;
    }
}
