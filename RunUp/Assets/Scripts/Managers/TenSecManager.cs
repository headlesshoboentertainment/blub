using UnityEngine;
using System.Collections.Generic;

public class TenSecManager : MonoBehaviour {

    public float IntervalBetweenNotifying;

    private List<GameObject> mObservers;

    private float mNextTimeToNotify;

	// Use this for initialization
	void Awake () {
        mObservers = new List<GameObject>();
        mNextTimeToNotify = Time.time + IntervalBetweenNotifying;
	}
	
	// Update is called once per frame
	void Update () {
        CalculateNextTimeNotify();
	}

    void CalculateNextTimeNotify()
    {
        if (mNextTimeToNotify < Time.time)
        {
            mNextTimeToNotify = Time.time + IntervalBetweenNotifying;
            NotifyObservers();
        }
    }

    

    public void Register(GameObject pGameobject)
    {
        mObservers.Add(pGameobject);
    }

    void NotifyObservers()
    {
        foreach (GameObject tObserver in mObservers)
        {
            tObserver.SendMessage("TenSec");
        }
    }

    public void UnRegister(GameObject pGameobject)
    {
        mObservers.Remove(pGameobject);
    }
}
