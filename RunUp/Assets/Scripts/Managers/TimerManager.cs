using UnityEngine;
using System.Collections;

public class TimerManager : MonoBehaviour {

    public static float sTheTimeSinceGameStart { get; set; }

    void Awake()
    {
        
    }

	// Use this for initialization
	void Start () {
        sTheTimeSinceGameStart = 0;
	}

    public void Reset()
    {
        sTheTimeSinceGameStart = 0;
    }
	
	// Update is called once per frame
	void Update () {
        Count();
	}

    void Count()
    {
        sTheTimeSinceGameStart += Time.deltaTime;
    }
}
