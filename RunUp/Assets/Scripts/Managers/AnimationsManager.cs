using UnityEngine;
using System.Collections;

[System.Serializable]
public class AnAnimation
{
    public string Name;
    public Material[] Materials;
    public AnimationType AnimationType;
    public float FrameRate;
    public bool PlayOnAwake;
    public float TimeToWaitBeforePlay;
}

public enum AnimationType
{
    LoopForward,
    LoopBackwards,
    Loop,
    Forward,
    Backwards,
    LoopRandom,
    ForwardNBackwards,
    Nothing
}





public class AnimationsManager : MonoBehaviour
{

    public AnAnimation[] Animations;

    public bool Finish { get; set; }

    public bool PlayingBackWards { get; set; }
    public bool PlayingNormal { get; set; }


    public Renderer cRenderer { get; set; }

    private AnimationType mAnimationType;

    private AnAnimation mCurrentAnimation;

    private float mTimeBetweenMaterials;

    private Material[] mMaterials;

    private bool Stop;


    // Use this for initialization
    void Start()
    {
        Finish = true;
        cRenderer = renderer;
        StartCoroutine(PlayAnimationOnAwake());
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void StopAnimation()
    {

        Stop = true;
        mAnimationType = AnimationType.Nothing;
        mCurrentAnimation = null;
        mMaterials = null;
    }

    public void StartAnimationBasedOnName(string pName)
    {
        foreach (AnAnimation tAnAnimation in Animations)
        {
            if (tAnAnimation.Name == pName)
            {
                MakeAnimation(tAnAnimation);
            }
        }
    }

    public float HowLongDoesAnimationTake(string pName)
    {
        float tHowLongItTakes = 0;

        foreach (AnAnimation tAnAnimation in Animations)
        {
            if (tAnAnimation.Name == pName)
            {
                tHowLongItTakes = tAnAnimation.Materials.Length * CalculateTimeBetweenMaterials(tAnAnimation.FrameRate);
            }
        }

        return tHowLongItTakes;
    }

    public bool HasAnimation(string pName)
    {
        bool tHasanimation = false;

        foreach (AnAnimation tAnAnimation in Animations)
        {
            if (tAnAnimation.Name == pName)
            {
                tHasanimation = true;
            }
        }

        return tHasanimation;
    }


    IEnumerator PlayAnimationOnAwake()
    {
        foreach (AnAnimation tAnAnimation in Animations)
        {
            if (tAnAnimation.PlayOnAwake)
            {
                if (mCurrentAnimation != null)
                {
                    yield return new WaitForSeconds(HowLongDoesAnimationTake(tAnAnimation.Name));
                }
                yield return new WaitForSeconds(tAnAnimation.TimeToWaitBeforePlay);
                MakeAnimation(tAnAnimation);
            }
        }
    }

    public AnAnimation CurrentAnimation()
    {
        return mCurrentAnimation;
    }

    float CalculateTimeBetweenMaterials(float pFrameRate)
    {
        float tTimeBetweenMaterials = 0;

        tTimeBetweenMaterials = 1 / pFrameRate;

        return tTimeBetweenMaterials;
    }

    public void MakeAnimation(AnAnimation pAnAnimation)
    {
        StartCoroutine(Animation(pAnAnimation));
    }

    private IEnumerator Animation(AnAnimation pAnAnimation)
    {
        if (pAnAnimation != null && pAnAnimation.Materials.Length > 0)
        {
            Stop = false;
            mCurrentAnimation = pAnAnimation;
            mMaterials = mCurrentAnimation.Materials;
            mTimeBetweenMaterials = CalculateTimeBetweenMaterials(mCurrentAnimation.FrameRate);
            mAnimationType = mCurrentAnimation.AnimationType;
            cRenderer = renderer;


            Finish = false;
            while (mAnimationType == AnimationType.Loop)
            {

                for (int i = 0; i < mMaterials.Length; i++)
                {
                    if (mAnimationType == AnimationType.Loop)
                    {
                        if (Stop)
                        {
                            break;
                        }
                        cRenderer.material = mMaterials[i];
                        yield return new WaitForSeconds(mTimeBetweenMaterials);
                    }


                }
                for (int i = mMaterials.Length - 1; i >= 0; i--)
                {
                    if (mAnimationType == AnimationType.Loop)
                    {
                        if (Stop)
                        {
                            break;
                        }
                        cRenderer.material = mMaterials[i];
                        yield return new WaitForSeconds(mTimeBetweenMaterials);
                    }
                }
                Finish = true;
            }

            while (mAnimationType == AnimationType.LoopForward)
            {

                for (int i = 0; i < mMaterials.Length; i++)
                {
                    if (mAnimationType == AnimationType.LoopForward)
                    {
                     
                        cRenderer.material = mMaterials[i];
                        yield return new WaitForSeconds(mTimeBetweenMaterials);

                    }
                }
                Finish = true;
            }

            while (mAnimationType == AnimationType.LoopBackwards)
            {
                for (int i = mMaterials.Length - 1; i >= 0; i--)
                {
                    if (mAnimationType == AnimationType.LoopBackwards)
                    {
                        if (Stop)
                        {
                            break;
                        }
                        cRenderer.material = mMaterials[i];
                        yield return new WaitForSeconds(mTimeBetweenMaterials);
                    }
                }
                Finish = true;
            }





            if (mAnimationType == AnimationType.Backwards)
            {
                PlayingBackWards = true;
                for (int i = mMaterials.Length - 1; i >= 0; i--)
                {
                    cRenderer.material = mMaterials[i];
                    yield return new WaitForSeconds(mTimeBetweenMaterials);

                }

                Finish = true;
            }
            else if (mAnimationType == AnimationType.Forward)
            {

                PlayingNormal = true;
                for (int i = 0; i < mMaterials.Length; i++)
                {
                    cRenderer.material = mMaterials[i];
                    yield return new WaitForSeconds(mTimeBetweenMaterials);

                }
                Finish = true;
            }

            if (mAnimationType == AnimationType.ForwardNBackwards)
            {

                PlayingNormal = true;
                for (int i = 0; i < mMaterials.Length; i++)
                {
                    cRenderer.material = mMaterials[i];
                    yield return new WaitForSeconds(mTimeBetweenMaterials);

                }


                PlayingBackWards = true;
                for (int i = mMaterials.Length - 1; i >= 0; i--)
                {
                    cRenderer.material = mMaterials[i];
                    yield return new WaitForSeconds(mTimeBetweenMaterials);

                }


                Finish = true;
            }

            while (mAnimationType == AnimationType.LoopRandom)
            {
                int tRandomNumber = Random.Range(0, mMaterials.Length);
                cRenderer.material = mMaterials[tRandomNumber];
                yield return new WaitForSeconds(mTimeBetweenMaterials);
            }


            PlayingBackWards = false;
            PlayingNormal = false;
        }



    }
}


