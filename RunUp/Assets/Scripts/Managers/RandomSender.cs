using UnityEngine;
using System.Collections.Generic;

public class RandomSender : MonoBehaviour {

    private List<GameObject> mObservers;


	// Use this for initialization
	void Awake () {
        mObservers = new List<GameObject>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void Register(GameObject pGameobject)
    {
        if (pGameobject != null)
        {
            mObservers.Add(pGameobject);
        }
    }

    void NotifyObservers()
    {
        foreach (GameObject tObserver in mObservers)
        {
            tObserver.SendMessage("RandomLaneGenerated");
        }
    }

    public void UnRegister(GameObject pGameobject)
    {
        mObservers.Remove(pGameobject);
    }

    
}
