using UnityEngine;
using System.Collections.Generic;

public class RandomGenerator : MonoBehaviour {


    public LayerMask LayerToHit;
    public Camera Camera;
    public GameObject[] SpawnLanes;

    private GameObject mCurrentlyParentObject;

    private RandomSender mRandomSender;

    private List<GameObject> mSpawnedChildObjects;

	// Use this for initialization
	void Start () {
        mSpawnedChildObjects = new List<GameObject>();
        mRandomSender = GameObject.FindGameObjectWithTag("RandomSender").GetComponent<RandomSender>();
	}
	
	// Update is called once per frame
	void Update () {
        
        if (!HittingBoxLane())
        {
            SpawnLane();
        }
        
	}

    void SpawnLane()
    {
        GameObject tGameobjectToSpawn = ChooseLaneToSpawn();
        Instantiate(tGameobjectToSpawn, new Vector3(0, CalculateCameraHighestYPoint(), 0), Quaternion.identity);
        mSpawnedChildObjects.Add(tGameobjectToSpawn);
        mRandomSender.SendMessage("NotifyObservers");

    }

    GameObject ChooseLaneToSpawn()
    {
        GameObject tLaneToSpawn = null;

        tLaneToSpawn = ChooseGameobject();
        
        return tLaneToSpawn;
    }

    GameObject ChooseGameobject()
    {
        GameObject tGameobject = null;

        ChildTrack tCurrentChildTrack = CurrentTrack().GetComponent<ChildTrack>();

        GameObject[] tArrayOfObjectToChooseFrom = FindTracksWithEntrance(tCurrentChildTrack.OutEntrance).ToArray();

        float tRandomNumber = Random.Range(0, tArrayOfObjectToChooseFrom.Length);
        tGameobject = tArrayOfObjectToChooseFrom[(int)tRandomNumber];

        return tGameobject;
    }

    List<GameObject> FindTracksWithEntrance(Entrance pEntrance)
    {
        List<GameObject> tEntrances = new List<GameObject>();

        foreach (GameObject tParentObject in SpawnLanes)
        {
            if (tParentObject.GetComponent<Track>().IntroEntrance == pEntrance)
            {
                foreach (Transform tChild in tParentObject.transform)
                {
                    tEntrances.Add(tChild.gameObject);
                }
            }
        }

        return tEntrances;
    }

    GameObject CurrentTrack()
    {
        GameObject tCurrentTrack = null;

        RaycastHit tHitInfo;
        if (Physics.Raycast(Camera.transform.position, Vector3.forward, out tHitInfo, 10,LayerToHit) &&
            tHitInfo.collider.tag == "BoxLane")
        {
            tCurrentTrack = tHitInfo.collider.transform.parent.gameObject;
        }

        return tCurrentTrack;
    }

   

    bool HittingBoxLane()
    {
        bool tHittingBoxLane = false;

        RaycastHit tHitInfo;
        if (Physics.Raycast(new Vector3(0, CalculateCameraHighestYPoint(),-10), Vector3.forward, out tHitInfo, 10))
        {
            if (tHitInfo.collider.tag == "BoxLane")
            {

                tHittingBoxLane = true;
            }
            else
            {
                print("NOT HITTHING BOX LANE");
            }
        }

        return tHittingBoxLane;
    }

    float CalculateCameraHighestYPoint()
    {
        float tHighestCameraYPoint = 0;

        tHighestCameraYPoint = Camera.transform.position.y + Camera.main.orthographicSize;

        return tHighestCameraYPoint;
    }
}
