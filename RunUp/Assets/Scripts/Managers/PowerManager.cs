using UnityEngine;
using System.Collections;

public class PowerManager : MonoBehaviour {

    public int FullPower;
    public int MinPower;

    public float SpeedIncrease;

    private int mPower { get; set; }

    private HealthBar mPowerBar;
    private CharactorSpeedUp mCharactorSpeedUp;


	// Use this for initialization
	void Start () {
        mPowerBar = GameObject.Find("HealthBar").GetComponent<HealthBar>();
        mCharactorSpeedUp = GameObject.FindGameObjectWithTag("Player").GetComponent<CharactorSpeedUp>();
        Reset();        
	}

    void Reset()
    {
        mPower = 0;

    }
	
	// Update is called once per frame
	void Update () {
        IsFullPower();
	}

    void IsFullPower()
    {
        if (mPower >= FullPower)
        {
            ActiveEffect();
            Reset();
        }
    }

    public void AddPower(int pPower)
    {
        if ((mPower + pPower) <= FullPower)
        {
            mPower += pPower;
            mPowerBar.Grow(pPower);
        }
        else
        {
            mPower = 100;
        }
    }

    public void RemovePower(int pPower)
    {
        if ((mPower - pPower) >= MinPower)
        {

            mPower -= pPower;
            mPowerBar.Shrink(pPower);
        }
        else
        {
            mPower = 0;
        }
    }



    void ActiveEffect()
    {
        mCharactorSpeedUp.DisableAll();
        mCharactorSpeedUp.ActivateSpeedUp(SpeedIncrease);
    }

}
