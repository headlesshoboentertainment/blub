using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour
{

    public AudioSource[] Sounds;
    public AudioSource[] Music;

    void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public  void PlaySound(string Name)
    {
        foreach (AudioSource tSound in Sounds)
        {
            if(tSound.name == Name)
            {
                
                tSound.Play();
            }
        }
    }

    public bool IsSoundPlaying(string Name)
    {
        bool tIsSoundPlaying = false;
        foreach (AudioSource tSound in Sounds)
        {
            if (tSound.name == Name)
            {
                if(tSound.isPlaying)
                {
                    tIsSoundPlaying = true;
                }
            }
        }
        return tIsSoundPlaying;
    }

    public void StopSound(string Name)
    {
        foreach (AudioSource tSound in Sounds)
        {
            if (tSound.name == Name)
            {
                tSound.Stop();
            }
        }
    }
}
