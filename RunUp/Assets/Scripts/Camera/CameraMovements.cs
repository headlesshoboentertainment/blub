using UnityEngine;
using System.Collections;

public class CameraMovements : MonoBehaviour {

    private GameObject mPlayer;

	// Use this for initialization
	void Start () {
        mPlayer = GameObject.FindGameObjectWithTag("Player");
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = new Vector3(transform.position.x, mPlayer.transform.position.y, transform.position.z);
	}
}
