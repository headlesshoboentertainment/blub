using UnityEngine;
using System.Collections;

public class SpeedSaver : PickupItem {
    
    public float SpeedIncreaseProcent;

	
	// Update is called once per frame
	void Update () {
	
	}

    protected override void OnTriggerEnter(Collider pCollider)
    {
        if (pCollider.tag == "Player")
        {
            WriteTextToPlayer();
            PickupInfo tPickupInfo = PickupInfos[0];
            GameObject.Find(tPickupInfo.ObjectNameOrTag).SendMessage(tPickupInfo.MethodToCall, SpeedIncreaseProcent);
            Die();
        }


    }
}
