using UnityEngine;
using System.Collections;

public class Note : PickupItem {

    public float PowerToGive;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    protected override void OnTriggerEnter(Collider pCollider)
    {
        if (pCollider.tag == "Player")
        {
            GameObject.Find("SoundManager").GetComponent<SoundManager>().PlaySound("Coin");
            WriteTextToPlayer();
            PickupInfo tPickupInfo = PickupInfos[1];
            GameObject.Find(tPickupInfo.ObjectNameOrTag).SendMessage(tPickupInfo.MethodToCall, PowerToGive);
        }
        if (pCollider.tag == "Player")
        {
            foreach (PickupInfo tPickupInfo in PickupInfos)
            {
                if (tPickupInfo.TagOrName == TOP.Tag && tPickupInfo.ObjectNameOrTag != "PowerManager")
                {
                    GameObject.FindGameObjectWithTag(tPickupInfo.ObjectNameOrTag).SendMessage(tPickupInfo.MethodToCall, SendMessageOptions.DontRequireReceiver);
                }


                else if (tPickupInfo.TagOrName == TOP.Name && tPickupInfo.ObjectNameOrTag != "PowerManager")
                {

                    GameObject.Find(tPickupInfo.ObjectNameOrTag).SendMessage(tPickupInfo.MethodToCall, SendMessageOptions.DontRequireReceiver);

                }
            }
            Die();
        }

    }


}
