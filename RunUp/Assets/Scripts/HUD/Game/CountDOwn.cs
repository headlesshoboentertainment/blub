using UnityEngine;
using System.Collections;

public class CountDOwn : MonoBehaviour {

    public float CountDownFrom;
    public float Interval;

    private counter mCounter;

    private float mCurrentNumber;

    private int mNextTimeToUpdate;

	// Use this for initialization
	void Start () {
        mCounter = GameObject.FindGameObjectWithTag("Counter").GetComponent<counter>();
        CalculateNextTimeToUpdate();
        mCurrentNumber = CountDownFrom;
        UpdateCounter();
	}

    void Reset()
    {
        mCurrentNumber = CountDownFrom;
    }
	
	// Update is called once per frame
	void Update () {
        ShouldUpdateCounter();
	}


    void ShouldUpdateCounter()
    {
        if (mNextTimeToUpdate < Time.time)
        {
            NumberDown();
            UpdateCounter();
            CalculateNextTimeToUpdate();
            
        }
    }

    void NumberDown()
    {
        if ((mCurrentNumber - 1) <= 0)
        {
            UpdateCounter();
            Reset();
        }
        else
        {
            mCurrentNumber--;
        }
    }

    void ShouldReset()
    {

    }


    void CalculateNextTimeToUpdate()
    {
        mNextTimeToUpdate = (int)(Time.time + Interval);
    }

    void UpdateCounter()
    {
      
        mCounter.CountDown(mCurrentNumber);
    }
}
