using UnityEngine;
using System.Collections;

public class TimerText : MonoBehaviour {
    public string WhatToWrite;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        WriteText();
	}

    void WriteText()
    {
        GetComponent<TextMesh>().text = WhatToWrite + TimerManager.sTheTimeSinceGameStart.ToString("F1");
    }
}
