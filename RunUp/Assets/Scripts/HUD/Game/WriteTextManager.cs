using UnityEngine;
using System.Collections;

public class WriteTextManager : MonoBehaviour {

    private static TextMesh sTextMessageToPlayer;

	// Use this for initialization
	void Start () {
        sTextMessageToPlayer = GameObject.FindGameObjectWithTag("TextHUD").GetComponent<TextMesh>();

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public static void WriteTextToPlayer(string pTheTextToWriteToPlayer)
    {
        sTextMessageToPlayer.text = pTheTextToWriteToPlayer;
    }
}
