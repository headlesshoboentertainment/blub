using UnityEngine;
using System.Collections;

public class counter : MonoBehaviour {

    public Material[] Materials;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void CountDown(float pCountDownNumber)
    {
        SetMaterial(pCountDownNumber);
    }

    void SetMaterial(float pCountDownNumber)
    {
        if ((10 - (int)pCountDownNumber) < 0)
        {
            renderer.material = Materials[0];
        }
        else if ((10 - (int)pCountDownNumber) > 0)
        {
            renderer.material = Materials[10];
        }
        renderer.material = Materials[10 - (int)pCountDownNumber];
    }
}
