using UnityEngine;
using System.Collections;

public enum ButtonType
{
    Start,
    Exit
}

public class Button : MonoBehaviour {

    public Vector3 ScaleUpBy;
    public float ScaleTime;

    public string LevelToLoad;
    public ButtonType MyButtonType;

    private Vector3 mStartScale;

    

	// Use this for initialization
	void Start () {
        mStartScale = transform.lossyScale;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void ScaleUp()
    {
        iTween.ScaleTo(gameObject, mStartScale + ScaleUpBy, ScaleTime);
    }

    void ScaleDown()
    {
        iTween.ScaleTo(gameObject, mStartScale, ScaleTime);
    }

    void OnMouseOver()
    {
        ScaleUp();
    }

    void OnMouseExit()
    {
        ScaleDown();
    }

    void OnMouseDown()
    {
        if (MyButtonType == ButtonType.Start)
        {
            Application.LoadLevel(LevelToLoad);
        }
        else
        {
            Application.Quit();
        }
    }
}
