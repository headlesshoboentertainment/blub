using UnityEngine;
using System.Collections;

public class BackgroundDown : MonoBehaviour {

    public float Speed;

    public Camera Camera;
    public GameObject Background;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        ShouldSpawnNewBackground();
        MoveDown();
	}

    void MoveDown()
    {
        transform.Translate(Vector3.down * Speed * Time.deltaTime);
    }

    void ShouldSpawnNewBackground()
    {
        if (!HittingBoxLane())
        {
            SpawnNewBackground();
        }
    }

    void SpawnNewBackground()
    {
        Instantiate(Background,new Vector3(transform.position.x,CalculateCameraHighestYPoint(),transform.position.z),transform.rotation);
    }

    bool HittingBoxLane()
    {
        bool tHittingBoxLane = false;

        RaycastHit tHitInfo;
        if (Physics.Raycast(new Vector3(0, CalculateCameraHighestYPoint(), -10), Vector3.forward, out tHitInfo, 1000))
        {
            tHittingBoxLane = true;           
        }

        else
        {
            print("NOT HITTHING BOX LANE");
        }

        return tHittingBoxLane;
    }


    float CalculateCameraHighestYPoint()
    {
        float tHighestCameraYPoint = 0;

        tHighestCameraYPoint = Camera.transform.position.y + Camera.main.orthographicSize;

        return tHighestCameraYPoint;
    }

}
