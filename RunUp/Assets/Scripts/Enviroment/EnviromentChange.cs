using UnityEngine;
using System.Collections;

public class EnviromentChange : MonoBehaviour {

    public Material[] Materials;

    private Material mCurrentMaterial;
    private TenSecManager TenSecManager;

    private RandomSender mRandomSender;

	// Use this for initialization
	void Start () {
        TenSecManager = GameObject.FindGameObjectWithTag("TenSecManager").GetComponent<TenSecManager>();
        TenSecManager.Register(gameObject);
        mCurrentMaterial = Materials[0];
        mRandomSender = GameObject.FindGameObjectWithTag("RandomSender").GetComponent<RandomSender>();
        mRandomSender.Register(gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	}

    void SetMaterial()
    {
        foreach (GameObject tNormalBlock in GameObject.FindGameObjectsWithTag("Normal"))
        {
            tNormalBlock.renderer.material = mCurrentMaterial;
        }
    }

   

   
}
