using UnityEngine;
using System.Collections;

public enum Theme
{
    Frost,
    Fire,
    Industry
}

[System.Serializable]
public class ThemeInfo
{
    public Theme Theme;
    public Material [] Materials;
    public string[] Tags;
}

public class EnviromentManager : MonoBehaviour {

    public ThemeInfo[] MaterialInfos;

    private TenSecManager mTenSecManager;
    private RandomSender mRandomSender;

    private Theme mTheme;
    private Theme mLastTheme;

	// Use this for initialization
	void Start () {
        mTenSecManager = GameObject.FindGameObjectWithTag("TenSecManager").GetComponent<TenSecManager>();
        mTenSecManager.Register(gameObject);

        mRandomSender = GameObject.FindGameObjectWithTag("RandomSender").GetComponent<RandomSender>();
        mRandomSender.Register(gameObject);

        mTheme = Theme.Fire;
        TenSec();
	}

	
	// Update is called once per frame
	void Update () {
        SetMaterial();
	}

    void TenSec()
    {
        mTheme = MakeSureRandomTheme();
        WriteTextManager.WriteTextToPlayer(mTheme.ToString() + " mode");
    }

    Theme MakeSureRandomTheme()
    {
        Theme tRandomTheme = RandomTheme();

        while (mTheme == tRandomTheme)
        {
            tRandomTheme = RandomTheme();
        }

        return tRandomTheme;
    }



    Theme RandomTheme()
    {
        Theme tTheme = Theme.Fire;

        float tRandomNumber = Random.Range(0, Theme.GetNames(typeof(Theme)).Length);

        tTheme = (Theme)tRandomNumber;

        return tTheme;
    }

    void SetMaterial()
    {
        foreach (ThemeInfo tThemeInfo in MaterialInfos)
        {
            if (tThemeInfo.Theme == mTheme)
            {
                for (int iMaterial = 0; iMaterial < tThemeInfo.Materials.Length; iMaterial++)
                {
                    for (int iTag = 0; iTag < tThemeInfo.Tags.Length;iTag++)
                    {
                        if (iTag == iMaterial)
                        {                       
                        foreach (GameObject tObjectToSetMaterialOn in GameObject.FindGameObjectsWithTag(tThemeInfo.Tags[iTag]))
                        {
                            if (tObjectToSetMaterialOn.renderer.isVisible)
                            {
                                if (tObjectToSetMaterialOn.GetComponent<AnimationsManager>() == null)
                                {                                    
                                    tObjectToSetMaterialOn.renderer.material = tThemeInfo.Materials[iMaterial];
                                }
                                    tObjectToSetMaterialOn.SendMessage("TheTheme", mTheme, SendMessageOptions.DontRequireReceiver);
                               
                            }
                        }
                        }
                    }
                }
            }
        }
    }

    void RandomLaneGenerated()
    {
        SetMaterial();
    }


}
